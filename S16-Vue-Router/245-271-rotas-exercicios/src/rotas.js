import Vue from 'vue'
import Router from 'vue-router'
import Inicio from './components/Inicio'

import Menu from './components/template/Menu'
import MenuAlt from './components/template/MenuAlt'

// IMPORT PADRÃO
// import Usuario from './components/Usuario/Usuario'
// import UsuarioLista from './components/Usuario/UsuarioLista'
// import UsuarioDetalhe from './components/Usuario/UsuarioDetalhe'
// import UsuarioEditar from './components/Usuario/UsuarioEditar'

const Usuario = () => import(/*webpackChunkName: "usuario"*/'./components/Usuario/Usuario')
const UsuarioLista = () => import(/*webpackChunkName: "usuario"*/'./components/Usuario/UsuarioLista')
const UsuarioDetalhe = () => import('./components/Usuario/UsuarioDetalhe')
const UsuarioEditar = () => import('./components/Usuario/UsuarioEditar')

Vue.use(Router)

const router = new Router({
    mode: 'history', // o padrão é hash
    scrollBehavior(to, from, savePosition) {
        if (savePosition) {
            return savePosition
        } else {
            return { x: 0, y: 0 }
        }
    },
    routes: [{
        path: '/',
        // component: Inicio,
        components: {
            default: Inicio,
            menu: Menu
        },
        name: 'inicio'
    }, {
        path: '/usuario',
        // component: Usuario,
        components: {
            default: Usuario,
            menu: Menu,
            menuInferior: MenuAlt
        },
        props: true, // permite que os parâmetros da url seja enviado pela props
        children: [
            {path: '', component: UsuarioLista},
            {path: ':id', component: UsuarioDetalhe, props: true, 
                beforeEnter: (to, from, next) => {
                    console.log('Antes da rota - usuário detalhe')
                    next()
                }
            },
            {path: ':id/editar', component: UsuarioEditar, props: true, name: 'usuarioEditar'}
        ]
    }, {
        path: '/redirecionar',
        redirect: '/usuario'
    }, {
        path: '*',
        redirect: '/'
    }]
})

router.beforeEach((to, from, next) => {
    console.log('Antes das rotas - Global')
    next()
})

export default router