import Vue from 'vue'
export default new Vue({
    methods: {
        alterarGenero(genero) {
            this.$emit('alterarGenero', genero)
        },
        quantoGeneroMudar(callback) {
            this.$on('alterarGenero', callback)
        }
    }
})