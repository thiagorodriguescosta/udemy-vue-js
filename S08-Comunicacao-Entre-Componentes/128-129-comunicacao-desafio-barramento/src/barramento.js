import Vue from 'vue'
export default new Vue({
    methods: {
        selecionarUsuario(usuario) {
            this.$emit('selecionarUsuario', usuario)
        },
        usuarioSelecionado(callback) {
            this.$on('selecionarUsuario', callback)
        }
    }
})