
new Vue({
    el: "#app",
    data: {
        play: false,
        percentLifePlayer: 100,
        percentLifeMoster: 100,

        attackPlayer: 0,
        attackMoster: 0,
        heal: 0,

        winner: false,
        log: []
    },
    methods: {
        startGamePlay() {
            this.play = true
            this.resetGamePlay()
        },
        resetGamePlay() {
            this.percentLifeMoster = 100
            this.percentLifePlayer = 100
            this.log = []
            this.winner = false
        },
        normalAttack() {
            this.resetAllAttacks()
            this.attackPlayer = Math.floor(Math.random() * 11);
            this.mosterAttack()
        },
        especialAttack() {
            this.resetAllAttacks()
            this.attackPlayer = Math.floor(Math.random() * 25);
            this.mosterAttack()
        },
        healing() {
            this.resetAllAttacks()
            this.heal = Math.floor(Math.random() * 21);
            this.mosterAttack()
        },
        mosterAttack() {
            this.attackMoster = Math.floor(Math.random() * 21);
            this.gamePlay()
        },
        resetAllAttacks() {
            this.attackPlayer = 0
            this.heal = 0
            this.attackMoster = 0
        },
        gamePlay() {
            this.logAttacks()
            if (!this.hasWinner()) {
                this.setLifes()
            }
        },
        logAttacks() {
            if (!this.winner) {
                this.log.push(
                    {type: "mostro", attack: this.attackMoster},
                    {type: "jogador", attack: this.attackPlayer}
                )
            }
        },
        hasWinner() {
            if (this.percentLifeMoster - this.attackPlayer < 0) {
                this.winner = "player"
                this.percentLifeMoster = 0
                this.play = false;
            } else if (this.percentLifePlayer - this.attackMoster + this.heal < 0) {
                this.winner = "moster"
                this.percentLifePlayer = 0
                this.play = false;
            }

            return this.winner
        },
        setLifes() {
            this.percentLifeMoster -= this.attackPlayer
            const life = this.percentLifePlayer - this.attackMoster + this.heal
            if (life <= 100) {
                this.percentLifePlayer -= this.attackMoster;
                this.percentLifePlayer += this.heal;
            } else {
                this.percentLifePlayer = 100;
            }
        }
    }
})