export default {
    namespaced: true,
    state: {
        produtos: []
    },
    getters: {
        totalProdutos(state) {
            return state.produtos.map(p => p.quantidade * p.preco)
                .reduce((total, atual) => total + atual, 0)
        }
    },
    mutations: { // Responsavel UNICAMENTE por mudar DIRETAMENTE o state
        adicionarProduto(state, produto) {
            return state.produtos.push(produto)
        }
    },
    actions: { // Responsavel por chamar as MUTATIONS e pode aplicar uma logica de programação
        adicionarProduto(contex, payload) {
            setTimeout(() => {
                contex.commit('adicionarProduto', payload)
            }, 1000)
        }
    }
}