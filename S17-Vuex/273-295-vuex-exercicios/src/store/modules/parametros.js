export default {
    state: {
        quantidade: 2,
        preco: 32.43
    },
    mutations: { // Responsavel UNICAMENTE por mudar DIRETAMENTE o state
        setQuantidade(state, payload) {
            state.quantidade = payload
        },
        setPreco(state, payload) {
            state.preco = payload
        }
    }
}