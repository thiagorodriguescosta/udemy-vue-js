// new Vue({
//     el: '#desafio',
//     data: {
//         valor: 0,
//         resultado: 'Valor Diferente'
//     },
//     watch: {
//         valor(novo, antigo) {
//             if (novo == 37) {
//                 this.resultado = 'Valor Igual'
//             } else {
//                 this.resultado = 'Valor Diferente'
//             }
//         },
//         resultado(novo, antigo) {
//             setTimeout(() => {
//                 this.valor = 0
//                 this.resultado = 'Valor Diferente'
//             }, 5000);
//         }
//     }
// });

new Vue({
    el: '#desafio',
    data: {
        valor: 0
    },
    watch: {
        resultado() {
            setTimeout(() => {
                this.valor = 0
            }, 5000);
        }
    },
    computed: {
        resultado() {
            return this.valor == 37 ? 'Valor Igual' : 'Valor Diferente'
        }
    },
})