new Vue({
	el: '#desafio',
	data: {
		classeEfeito: "destaque",

		classeInformada: "",
		classeInformada2: "",

		altura: 100,
		largura: 100,
		cor: "red",
		blue: false,

		porcentagem: 0 + "%"
	},
	computed: {
		meuEstilo() {
			return {
				width: this.largura + 'px',
				height: this.altura + 'px',
				backgroundColor: this.cor
			}
		}
	},
	methods: {
		iniciarEfeito() {
			setInterval(() => {
				this.classeEfeito = this.classeEfeito == "destaque" ? "encolher" : "destaque"
			}, 1000)
		},
		iniciarProgresso() {
			let valor = 0
			const temporizador = setInterval(() => {
				valor += 5
				this.porcentagem = valor + "%"
				if (valor >= 100) {
					clearInterval(temporizador)
				}
			}, 500)
		},
		classeBlue(event) {
			if (event.target.value == "true") {
				this.blue = true;
			} else if (event.target.value == "false") {
				this.blue = false;
			}
		}
	}
})
