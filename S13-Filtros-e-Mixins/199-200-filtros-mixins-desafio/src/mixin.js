export default {
    computed: {
        computedTamanhoString() {
			const array = 'Pedro é legal'.split(' ')
			let i = 0
			while(array.length > i) {
				array[i] += " (" + array[i].length + ") "
				i++
			}
			return array.join('')
		}
    },
}