import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

Vue.filter('tamanhoString', function(valor){
	const array = valor.split(' ')
	let i = 0
	while(array.length > i) {
		array[i] += " (" + array[i].length + ") "
		i++
	}
	return array.join('')
})

new Vue({
	render: h => h(App),
}).$mount('#app')
