export default {
    data() {
        return {
            fruta: '',
            frutas: ['maçã', 'pêra', 'uva']
        }
    },
    methods: {
        add(event) {
            this.frutas.push(event.target.value)
        }
    },
}