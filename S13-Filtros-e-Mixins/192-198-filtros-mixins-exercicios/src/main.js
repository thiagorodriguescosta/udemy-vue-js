import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

Vue.filter('inverter', function(valor) {
	return valor.split('').reverse().join('')
})

Vue.mixin({
	data() {
		return {
			globalMixin: 'estou no mixin global'
		}
	},
	created() {	
		console.log('Created - Mixin global')
	}
})

new Vue({
	render: h => h(App)
}).$mount('#app')
