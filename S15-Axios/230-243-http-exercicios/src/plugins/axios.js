import Vue from 'vue'
import axios from 'axios'

// GLOBAL
// axios.defaults.baseURL = 'https://curso-vue-2ab2c.firebaseio.com/'
// axios.defaults.headers.common['Authorization'] = 'token123'
axios.defaults.headers.get['Accepts'] = 'application/json'

Vue.use({
    install(Vue) {
        Vue.prototype.$http = axios.create({
            baseURL: 'https://curso-vue-2ab2c.firebaseio.com/',
            headers: {
                "Authorization": 'token123'
            }
        })

        Vue.prototype.$http.interceptors.request.use(config => {
            // if (config.method == 'post') {
            //     config.method = 'put'
            // }
            // console.log(config.method)
            return config
        }, error => Promise.reject(error))

        // Vue.prototype.$http.interceptors.response.use(res => {
            // alterar para um array
            // const array = []
            // for(let chave in res.data) {
            //     array.push({id: chave, ...res.data[chave] })
            // }
            // res.data = array
            // return res
        // }, error => Promise.reject(error))
    }
})