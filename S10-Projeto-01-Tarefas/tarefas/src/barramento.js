import Vue from 'vue'
export default new Vue({
    methods: {
        changeStatusTask(task) {
            this.$emit('changeStatus', task)
        },
        changedStatusTask(callback) {
            this.$on('changeStatus', callback)
        },
        deleteTask(task) {
            this.$emit('delete', task)
        },
        deletedTask(callback) {
            this.$on('delete', callback)
        }
    },
})